#pragma once

#include "ofMain.h"
#include "ofxCv.h"
#include "ofEvents.h"
#include "ofxLibwebsockets.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
    
    	ofArduino	ard;
        bool		bSetupArduino;			// flag variable for setting up arduino once
    
        int windowW;
        int windowH;
        int grid_one;
        int grid_two;
        int grid_three;
        int grid_four;
    
        int debug_config = 1;
    
        int light_11 = 0;
        int brightness = 0;
        int fadeAmount = 5;
        int ledState = 0;
        long previousMillis = 0;
        long interval = 100;

        int physical_one = 0;
        int physical_two = 0;
        int physical_three = 0;
        int physical_four = 0;
    
        int socket_one = 0;
        int socket_two = 0;
        int socket_three = 0;
        int socket_four = 0;
    
        int return_one = 0;
        int return_two = 0;
        int return_three = 0;
        int return_four = 0;
    
        void turnOnLight(int pin);
    
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
        void setGrid(int w, int h);
        void sendUpdate(int one, int two, int three, int four, int source);
    
        ofVideoGrabber cam;
        ofxCv::ContourFinder contourFinder;
        float threshold;
        ofxCv::TrackingColorMode trackingColorMode;
        ofColor targetColor;
    
        ofxLibwebsockets::Client client;
        
        // websocket methods
        void onConnect( ofxLibwebsockets::Event& args );
        void onOpen( ofxLibwebsockets::Event& args );
        void onClose( ofxLibwebsockets::Event& args );
        void onIdle( ofxLibwebsockets::Event& args );
        void onMessage( ofxLibwebsockets::Event& args );
        void onBroadcast( ofxLibwebsockets::Event& args );
    
    private:
        
        void setupArduino(const int & version);
        void digitalPinChanged(const int & pinNum);
        void analogPinChanged(const int & pinNum);
        void updateArduino();
        
        string buttonState;
        string potValue;
    
		
};
