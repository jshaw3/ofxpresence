#include "ofApp.h"

using namespace ofxCv;
using namespace cv;

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetLogLevel("loggingChannelStuff", OF_LOG_ERROR);
    
    // Editable For Prototype
    
    // targetColor = ofColor(171, 101, 94, 255);
//    targetColor = ofColor(63, 45, 36, 255);
//    blue shirt
    targetColor = ofColor(84, 85, 99, 255);
//    targetColor = ofColor(122, 140, 134, 255);

    // evening test face color = 63, 45, 36, 255
    
    // threshold = 70;
    threshold = 39;
    // evening test = 39
    
    contourFinder.setTargetColor(targetColor, trackingColorMode);
    
    cam.setDeviceID(0);
    contourFinder.setMinAreaRadius(50);
    contourFinder.setMaxAreaRadius(500);
    
    ofSetFrameRate(30);
    
    // openCV stuff
    // CAM INPUT
    cam.initGrabber(640, 480);
    trackingColorMode = TRACK_COLOR_RGB;
    
    vector< ofVideoDevice > devices = cam.listDevices();
    for(int i = 0; i < devices.size(); i++) {
        cout << "Device ID: " << devices[i].id << " Device Name: " << devices[i].deviceName << " Hardware Name: " << devices[i].hardwareName << " Is Available: " << devices[i].bAvailable << endl;
    }
    
    // WebSocket Stuff
    ofBackground(0);
    
    windowW = ofGetWindowWidth();
    windowH = ofGetWindowHeight();
    
    setGrid(windowW, windowH);
    
    // 1 - get default options
    ofxLibwebsockets::ClientOptions options = ofxLibwebsockets::defaultClientOptions();
    
    // 2 - set basic params
    options.host = "localhost";
    options.port = 8000;
    options.protocol = "ws://";
    options.bUseSSL = false;
    
    
    // 3 - set keep alive params
    // BIG GOTCHA: on BSD systems, e.g. Mac OS X, these time params are system-wide
    // ...so ka_time just says "check if alive when you want" instead of "check if
    // alive after X seconds"
    // Note: some have had issues with only setting one of these; may be an
    // all-or-nothing type option!
    options.ka_time     = 10;
    options.ka_probes   = 10;
    options.ka_interval = 10;
    
    // 4 - connect
    client.connect(options);
    
    client.addListener(this);
    
    ard.connect("/dev/cu.usbmodem1411", 57600);
    
    ofAddListener(ard.EInitialized, this, &ofApp::setupArduino);
    bSetupArduino	= false;	// flag so we setup arduino when its ready, you don't need to touch this :)
}

//--------------------------------------------------------------
void ofApp::update(){
    
    updateArduino();
    
    cam.update();
    if(cam.isFrameNew()) {
        if(debug_config == 1){
            threshold = ofMap(mouseX, 0, ofGetWidth(), 0, 255);
        }
        contourFinder.setThreshold(threshold);
        contourFinder.findContours(cam);
    }
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofDrawBitmapString("Type anywhere to send 'hello' to your server\nCheck the console for output!", 10,20);
    ofDrawBitmapString(client.isConnected() ? "Client is connected" : "Client disconnected :(", 10,50);
    
    
    //    openCV stuff
    ofSetColor(255);
    cam.draw(0, 0);
    
    ofSetLineWidth(2);
    contourFinder.draw();
    
    ofNoFill();
    int n = contourFinder.size();
    
    for(int i = 0; i < n; i++) {
        // smallest rectangle that fits the contour
        ofSetColor(cyanPrint);
        ofPolyline minAreRect = toOf(contourFinder.getMinAreaRect(i));
        minAreRect.draw();
        
        // ellipse that best fits the contour
        ofSetColor(magentaPrint);
        cv::RotatedRect ellipse = contourFinder.getFitEllipse(i);
        ofPushMatrix();
        ofVec2f ellipseCenter = toOf(ellipse.center);
        ofVec2f ellipseSize = toOf(ellipse.size);
        ofTranslate(ellipseCenter.x, ellipseCenter.y);
        ofRotate(ellipse.angle);
        ofEllipse(0, 0, ellipseSize.x, ellipseSize.y);
        ofPopMatrix();
        
        // minimum area circle that encloses the contour
        ofSetColor(cyanPrint);
        float circleRadius;
        ofVec2f circleCenter = toOf(contourFinder.getMinEnclosingCircle(i, circleRadius));
        ofCircle(circleCenter, circleRadius);
        
        // convex hull of the contour
        ofSetColor(yellowPrint);
        ofPolyline convexHull = toOf(contourFinder.getConvexHull(i));
        convexHull.draw();
        
        // defects of the convex hull
        vector<cv::Vec4i> defects = contourFinder.getConvexityDefects(i);
        for(int j = 0; j < defects.size(); j++) {
            ofLine(defects[j][0], defects[j][1], defects[j][2], defects[j][3]);
        }
        
        // some different styles of contour centers
        ofVec2f centroid = toOf(contourFinder.getCentroid(i));
        ofVec2f average = toOf(contourFinder.getAverage(i));
        ofVec2f center = toOf(contourFinder.getCenter(i));
        ofSetColor(cyanPrint);
        ofCircle(centroid, 1);
        ofSetColor(magentaPrint);
        ofCircle(average, 1);
        ofSetColor(yellowPrint);
        ofCircle(center, 1);
        
        // you can also get the area and perimeter using ofPolyline:
        // ofPolyline::getArea() and ofPolyline::getPerimeter()
        double area = contourFinder.getContourArea(i);
        double length = contourFinder.getArcLength(i);
        
        // balance is useful for detecting when a shape has an "arm" sticking out
        // if balance.length() is small, the shape is more symmetric: like I, O, X...
        // if balance.length() is large, the shape is less symmetric: like L, P, F...
        ofVec2f balance = toOf(contourFinder.getBalance(i));
        ofPushMatrix();
        ofTranslate(centroid.x, centroid.y);
        ofScale(5, 5);
        ofLine(0, 0, balance.x, balance.y);
        ofPopMatrix();
        
        // THIS IS WHAT CHANGES THE LED
        // ard.sendDigital(13, ARD_LOW);
        // ard.sendDigital(12, ARD_LOW);
        // ard.sendDigital(11, ARD_LOW);
        // ard.sendDigital(10, ARD_LOW);
        
        int source = 0;
        
        if(ellipseCenter.x < grid_one){
            ofLog(OF_LOG_NOTICE, "grid_one %d", ellipseCenter.x);
            sendUpdate(1,return_two,return_three,return_four, source);
        } else if (ellipseCenter.x > grid_one && ellipseCenter.x < grid_two){
            ofLog(OF_LOG_NOTICE, "grid_two %d", ellipseCenter.x);
            sendUpdate(return_one,1,return_three,return_four, source);
        } else if (ellipseCenter.x > grid_two && ellipseCenter.x < grid_three){
            ofLog(OF_LOG_NOTICE, "grid_three %d", ellipseCenter.x);
            sendUpdate(return_one,return_two,1,return_three, source);
        } else if (ellipseCenter.x > grid_three && ellipseCenter.x < grid_four){
            ofLog(OF_LOG_NOTICE, "grid_four %d", ellipseCenter.x);
            sendUpdate(return_one,return_two,return_three,1, source);
        } else {
            ofLog(OF_LOG_NOTICE, "WHERE AM I? %d", ellipseCenter.x);
            client.send("?");
            ard.sendDigital(9, ARD_HIGH);
        }
    }
    
    ofSetColor(255);
    drawHighlightString(ofToString((int) ofGetFrameRate()) + " fps", 10, 10);
    drawHighlightString(ofToString((int) threshold) + " threshold", 10, 30);
    drawHighlightString(trackingColorMode == TRACK_COLOR_RGB ? "RGB tracking" : "hue tracking", 10, 50);
    
    ofTranslate(8, 75);
    ofFill();
    ofSetColor(0);
    ofRect(-3, -3, 64+6, 64+6);
    ofSetColor(targetColor);
    ofRect(0, 0, 64, 64);
    
    // Playing around with fading the LED seperate from the OFX draw loop
    // if(light_11 == 1){
    //  turnOnLight(11);
    // }
}

//--------------------------------------------------------------
void ofApp::setGrid(int w, int h){
    
    int start = 0;
    
    grid_one = (w / 4);
    grid_two = grid_one + grid_one;
    grid_three = grid_two + grid_one;
    grid_four = grid_three + grid_one;
    
    ofLog(OF_LOG_NOTICE, "grid_one %d", grid_one);
    ofLog(OF_LOG_NOTICE, "grid_two %d", grid_two);
    ofLog(OF_LOG_NOTICE, "grid_three %d", grid_three);
    ofLog(OF_LOG_NOTICE, "grid_four %d", grid_four);
}

//--------------------------------------------------------------
// Fading LED by Pin function / Still prototyping this
void ofApp::turnOnLight(int pin){
    
    long currentMillis = ofGetElapsedTimeMillis();
    
    if(currentMillis - previousMillis >= interval) {
        // save the last time you blinked the LED
        previousMillis = currentMillis;
        
        // change the brightness for next time through the loop:
        brightness = brightness + fadeAmount;
        
        // reverse the direction of the fading at the ends of the fade:
        if (brightness == 0 || brightness == 255) {
            fadeAmount = -fadeAmount;
            light_11 = 0;
        }
        
        // set the LED with the ledState of the variable:
        ard.sendPwm(pin, (int)(brightness));
    }
    
}

//--------------------------------------------------------------
void ofApp::onBroadcast( ofxLibwebsockets::Event& args ){
    cout<<"got broadcast "<<args.message<<endl;
}

//--------------------------------------------------------------
void ofApp::onConnect( ofxLibwebsockets::Event& args ){
    cout<<"on connected"<<endl;
}

//--------------------------------------------------------------
void ofApp::onOpen( ofxLibwebsockets::Event& args ){
    cout<<"on open"<<endl;
}

//--------------------------------------------------------------
void ofApp::onClose( ofxLibwebsockets::Event& args ){
    cout<<"on close"<<endl;
}

//--------------------------------------------------------------
void ofApp::onIdle( ofxLibwebsockets::Event& args ){
    cout<<"on idle"<<endl;
}

//--------------------------------------------------------------
void ofApp::onMessage( ofxLibwebsockets::Event& args ){
    cout<<"got message "<<args.message<<endl;
    // cout<<"got message "<<args.json<<endl;
    // cout<<"got message "<<args.data<<endl;
    
    // ard.sendDigital(13, ARD_LOW);
    // ard.sendDigital(12, ARD_LOW);
    // ard.sendDigital(11, ARD_LOW);
    // ard.sendDigital(10, ARD_LOW);
    
    vector<string> splitItems = ofSplitString(args.message, ",");
    cout << splitItems.size() << endl;
    cout << splitItems[0] << endl;
    
    if (splitItems.size() > 0) {
        sendUpdate(ofToInt(splitItems[0]), ofToInt(splitItems[1]), ofToInt(splitItems[2]), ofToInt(splitItems[3]), 1);
    }
    
}

//--------------------------------------------------------------
void ofApp::sendUpdate(int one, int two, int three, int four, int source){
    
        ofLog(OF_LOG_NOTICE, "SOURCE 1");
        socket_one = one;
        socket_two = two;
        socket_three = three;
        socket_four = four;
        
        if(socket_one == 0 && physical_one == 0){
            return_one = 0;
            ard.sendDigital(10, ARD_LOW);
        } else {
            return_one = 1;
            ard.sendDigital(10, ARD_HIGH);
        }
        
        if(socket_two == 0 && physical_two == 0){
            return_two = 0;
            ard.sendDigital(11, ARD_LOW);
        } else {
            return_two = 1;
            ard.sendDigital(11, ARD_HIGH);
        }
        
        if(socket_three == 0 && physical_three == 0){
            return_three = 0;
            ard.sendDigital(12, ARD_LOW);
        } else {
            return_three = 1;
            ard.sendDigital(12, ARD_HIGH);
        }
        
        if(socket_four == 0 && physical_four == 0){
            return_four = 0;
            ard.sendDigital(13, ARD_LOW);
        } else {
            return_four = 1;
            ard.sendDigital(13, ARD_HIGH);
        }

    
    string str = ofToString(return_one) + "," + ofToString(return_two) + "," +  ofToString(return_three) + "," + ofToString(return_four);

    client.send(str);
    
    physical_one = 0;
    physical_two = 0;
    physical_three = 0;
    physical_four = 0;
    
    socket_one = 0;
    socket_two = 0;
    socket_three = 0;
    socket_four = 0;
    
    return_one = 0;
    return_two = 0;
    return_three = 0;
    return_four = 0;
    

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    string str = "keypressed: " + ofToString(key);
    
    if(key == 'h') {
        trackingColorMode = TRACK_COLOR_HS;
    }
    if(key == 'r') {
        trackingColorMode = TRACK_COLOR_RGB;
    }
    contourFinder.setTargetColor(targetColor, trackingColorMode);
    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    
    ard.sendDigital(13, ARD_LOW);
    ard.sendDigital(12, ARD_LOW);
    ard.sendDigital(11, ARD_LOW);
    ard.sendDigital(10, ARD_LOW);
    
    if(x < grid_one){
        client.send("1");
        light_11 = 1;
        ard.sendDigital(10, ARD_HIGH);
    } else if (x > grid_one && x < grid_two){
        client.send("2");
        ard.sendDigital(11, ARD_HIGH);
    } else if (x > grid_two && x < grid_three){
        client.send("3");
        ard.sendDigital(12, ARD_HIGH);
    } else if (x > grid_three && x < grid_four){
        client.send("4");
        ard.sendDigital(13, ARD_HIGH);
    } else {
        client.send("4");
        ard.sendDigital(10, ARD_HIGH);
    }
    
    if(debug_config == 1){
        targetColor = cam.getPixelsRef().getColor(x, y);
        cout<<"********* targetColor: "<<targetColor<<endl;
        contourFinder.setTargetColor(targetColor, trackingColorMode);
    }
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    ofLog(OF_LOG_WARNING, "Mouse button %i was pressed at x=%i, y=%i !", button, x, y);
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){
    
}



//--------------------------------------------------------------
void ofApp::setupArduino(const int & version) {
    
    // remove listener because we don't need it anymore
    ofRemoveListener(ard.EInitialized, this, &ofApp::setupArduino);
    
    // it is now safe to send commands to the Arduino
    bSetupArduino = true;
    
    // print firmware name and version to the console
    ofLogNotice() << ard.getFirmwareName();
    ofLogNotice() << "firmata v" << ard.getMajorFirmwareVersion() << "." << ard.getMinorFirmwareVersion();
    
    // Note: pins A0 - A5 can be used as digital input and output.
    // Refer to them as pins 14 - 19 if using StandardFirmata from Arduino 1.0.
    // If using Arduino 0022 or older, then use 16 - 21.
    // Firmata pin numbering changed in version 2.3 (which is included in Arduino 1.0)
    
    // set pins D2 and A5 to digital input
    //    ard.sendDigitalPinMode(2, ARD_INPUT);
    //    ard.sendDigitalPinMode(19, ARD_INPUT);  // pin 21 if using StandardFirmata from Arduino 0022 or older
    //
    //    // set pin A0 to analog input
    //    ard.sendAnalogPinReporting(0, ARD_ANALOG);
    //
    //    // set pin D13 as digital output
    //    ard.sendDigitalPinMode(13, ARD_OUTPUT);
    //    // set pin A4 as digital output
    //    ard.sendDigitalPinMode(18, ARD_OUTPUT);  // pin 20 if using StandardFirmata from Arduino 0022 or older
    //
    //    // set pin D11 as PWM (analog output)
//    ard.sendDigitalPinMode(11, ARD_PWM);
    //
    //    // attach a servo to pin D9
    //    // servo motors can only be attached to pin D3, D5, D6, D9, D10, or D11
    //    ard.sendServoAttach(9);
    
    // Listen for changes on the digital and analog pins
    ofAddListener(ard.EDigitalPinChanged, this, &ofApp::digitalPinChanged);
    ofAddListener(ard.EAnalogPinChanged, this, &ofApp::analogPinChanged);
}

//--------------------------------------------------------------
void ofApp::updateArduino(){
    
    // update the arduino, get any data or messages.
    // the call to ard.update() is required
    ard.update();
    
    // do not send anything until the arduino has been set up
    if (bSetupArduino) {
        // fade the led connected to pin D11
        //        ard.sendPwm(11, (int)(128 + 128 * sin(ofGetElapsedTimef())));   // pwm...
    }
    
}

// digital pin event handler, called whenever a digital pin value has changed
// note: if an analog pin has been set as a digital pin, it will be handled
// by the digitalPinChanged function rather than the analogPinChanged function.

//--------------------------------------------------------------
void ofApp::digitalPinChanged(const int & pinNum) {
    // do something with the digital input. here we're simply going to print the pin number and
    // value to the screen each time it changes
    buttonState = "digital pin: " + ofToString(pinNum) + " = " + ofToString(ard.getDigital(pinNum));
}

// analog pin event handler, called whenever an analog pin value has changed

//--------------------------------------------------------------
void ofApp::analogPinChanged(const int & pinNum) {
    // do something with the analog input. here we're simply going to print the pin number and
    // value to the screen each time it changes
    potValue = "analog pin: " + ofToString(pinNum) + " = " + ofToString(ard.getAnalog(pinNum));
}



